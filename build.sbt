name := "MovieAnalyzer"

version := "0.1"

scalaVersion := "2.12.10"

val sparkVersion = "3.0.1"
val scalaTestVersion = "3.0.1"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-sql" % sparkVersion % "compile",
  "org.apache.spark" %% "spark-streaming" % sparkVersion % "compile",
  "org.scalatest" %% "scalatest" % scalaTestVersion % "test"
)


