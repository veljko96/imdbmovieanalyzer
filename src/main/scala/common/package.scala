import org.apache.spark.sql.types.{DoubleType, IntegerType, StringType, StructField, StructType}

package object imdb {

  val ratingsSchema = StructType(Array(
    StructField("tconst", StringType, nullable = true),
    StructField("averageRating", DoubleType, nullable = true),
    StructField("numVotes", IntegerType, nullable = true)
  ))

  val nameBasicsSchema = StructType(Array(
    StructField("nconst", StringType, nullable = true),
    StructField("primaryName", StringType, nullable = true),
    StructField("birthYear", StringType, nullable = true),
    StructField("deathYear", StringType, nullable = true),
    StructField("primaryProfession", StringType, nullable = true),
    StructField("knownForTitles", StringType, nullable = true)
  ))

  val titleBasicsSchema = StructType(Array(
    StructField("tconst", StringType, nullable = true),
    StructField("titleType", StringType, nullable = true),
    StructField("primaryTitle", StringType, nullable = true),
    StructField("originalTitle", StringType, nullable = true),
    StructField("isAdult", StringType, nullable = true),
    StructField("startYear", StringType, nullable = true),
    StructField("endYear", StringType, nullable = true),
    StructField("runtimeMinutes", StringType, nullable = true),
    StructField("genres", StringType, nullable = true)
  ))

  val principalsSchema = StructType(Array(
    StructField("tconst", StringType, nullable = true),
    StructField("ordering", IntegerType, nullable = true),
    StructField("nconst", StringType, nullable = true),
    StructField("category", StringType, nullable = true),
    StructField("job", StringType, nullable = true),
    StructField("characters", StringType, nullable = true)
  ))

  val titleAkasSchema = StructType(Array(
    StructField("titleId", StringType, nullable = true),
    StructField("ordering", IntegerType, nullable = true),
    StructField("title", StringType, nullable = true),
    StructField("region", StringType, nullable = true),
    StructField("language", StringType, nullable = true),
    StructField("types", StringType, nullable = true),
    StructField("attributes", StringType, nullable = true),
    StructField("isOriginalTitle", StringType, nullable = true)
  ))

  val topTenMoviesSchema = StructType(Array(
    StructField("tconst", StringType),
    StructField("primaryTitle", StringType),
    StructField("ranking", DoubleType)
  ))
}
