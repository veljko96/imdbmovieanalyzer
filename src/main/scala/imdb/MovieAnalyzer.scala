package imdb

import helpers.ReadTransformStoreHelper
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{SaveMode, SparkSession}

object MovieAnalyzer extends App with ReadTransformStoreHelper{

  val LOG: Logger = Logger.getLogger(this.getClass)

  Logger.getLogger("org").setLevel(Level.INFO)

  if (args.length == 0) throw new IllegalArgumentException("Give me the params")

  val inputDataDirectory: String = args(0)
  val outputDataDirectory: String = args(1)

  val spark = SparkSession.builder()
    .appName("GameRating")
//    .master("local[*]")
    .getOrCreate()

  val allReadData = readData(spark, inputDataDirectory)

  val dfRatings = allReadData("ratingsDataFrame")
  dfRatings.cache()
  val dfBasicTitles = allReadData("titleBasicsDataFrame")

  val dfTopTenImdbMovies = getTopTenImdbMovies(dfRatings, dfBasicTitles)
  dfTopTenImdbMovies.cache()

  val dfTopTenMoviesWithMostCreditedPersons = getTheMostOftenCreditedPeople(dfTopTenImdbMovies, allReadData)

  val dfAllAltMovieTitles = getAllAltMovieTitles(dfTopTenImdbMovies, allReadData)

  storeData(dfTopTenImdbMovies, outputDataDirectory + "top_ten_movies/")
  storeData(dfTopTenMoviesWithMostCreditedPersons, outputDataDirectory + "most_credited_persons/")
  storeData(dfAllAltMovieTitles, outputDataDirectory + "alt_names/")


}
