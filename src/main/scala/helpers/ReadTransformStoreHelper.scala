package helpers

import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, Dataset, SaveMode, SparkSession}
import imdb.{nameBasicsSchema, ratingsSchema, titleBasicsSchema, titleAkasSchema, principalsSchema}

trait ReadTransformStoreHelper {

  def readData(spark: SparkSession, inputDataDirectory: String): Map[String, DataFrame] = {

    val dfRatings = createDF(spark, inputDataDirectory + "title.ratings.tsv.gz", ratingsSchema)

    val dfNameBasics = createDF(spark, inputDataDirectory + "name.basics.tsv.gz", nameBasicsSchema)

    val dfTitleBasics = createDF(spark, inputDataDirectory + "title.basics.tsv.gz", titleBasicsSchema)
      .where("titleType='movie'")

    val dfPrincipals = createDF(spark, inputDataDirectory + "title.principals.tsv.gz", principalsSchema)

    val dfTitleAkas = createDF(spark, inputDataDirectory + "title.akas.tsv.gz", titleAkasSchema)
      .where("isOriginalTitle=0")

    val allReadData = Map.newBuilder[String, DataFrame]

    allReadData.+=("ratingsDataFrame" -> dfRatings)
    allReadData.+=("nameBasicsDataFrame" -> dfNameBasics)
    allReadData.+=("titleBasicsDataFrame" -> dfTitleBasics)
    allReadData.+=("principalsDataFrame" -> dfPrincipals)
    allReadData.+=("titleAltNamesDataFrame" -> dfTitleAkas)

    allReadData.result()

  }

  def createDF(spark: SparkSession, inputDataDirectoryAndFile: String, schema: StructType): DataFrame = {
    spark.read
      //.schema(schema)
      .option("inferSchema", "true")
      .option("header", "true")
      .option("delimiter", "\t")
      .csv(inputDataDirectoryAndFile)
  }

  def getTopTenImdbMovies(dfRatings: DataFrame, dfBasicTitles: DataFrame): DataFrame = {
    // Calculate average number of votes for formula
    val dfAverageNumberOfVotes = dfRatings
      .agg(avg("numVotes").alias("averageNumberOfVotes"))
    val averageNumberOfVotes = dfAverageNumberOfVotes.first().getDouble(0)

    val dfRatingsMovieOnly = dfRatings
      .join(broadcast(dfBasicTitles), "tconst")

    val dfTopTenImdbMovies = dfRatingsMovieOnly
      .withColumn("ranking", round((col("numVotes")/lit(averageNumberOfVotes))*col("averageRating"), 2))
      .where("numVotes > 50")
      .orderBy(col("ranking").desc)
      .select("tconst","primaryTitle", "ranking")
      .limit(10)
    dfTopTenImdbMovies

  }

  def getTheMostOftenCreditedPeople(dfTopTenImdbMovies: DataFrame, allReadData: Map[String, DataFrame]): DataFrame = {

    val dfPrincipals = allReadData("principalsDataFrame")

    val dfTopTenMoviesWithPrincipals = dfPrincipals
      .join(broadcast(dfTopTenImdbMovies), "tconst")

    val dfNameBasics = allReadData("nameBasicsDataFrame")
    val dfTopTenMoviesWithNameBasics = dfNameBasics
      .join(broadcast(dfTopTenMoviesWithPrincipals), "nconst")
      .select("tconst", "nconst", "primaryName")

    val dfTheMostOftenCreditedPeople = dfTopTenMoviesWithNameBasics
        .groupBy("nconst", "primaryName")
        .agg(
          count(col("tconst")).as("numberOfCredits")
        )
        .sort(col("numberOfCredits").desc)

    dfTheMostOftenCreditedPeople

  }

  def getAllAltMovieTitles(dfTopTenImdbMovies: DataFrame, allReadData: Map[String, DataFrame]): DataFrame = {

    val dfTitleAkas = allReadData("titleAltNamesDataFrame")

    val dfTopTenImdbMoviesWithAltTitles = dfTitleAkas
      .join(broadcast(dfTopTenImdbMovies), dfTitleAkas("titleId") === dfTopTenImdbMovies("tconst"))
      .groupBy("tconst", "primaryTitle")
      .agg(concat_ws(",", collect_set(col("title"))).alias("altNames"))

    dfTopTenImdbMoviesWithAltTitles
  }

  def storeData(dataFrame: DataFrame, outputDir: String) = {
    dataFrame
      .coalesce(1)
      .write
      .mode(SaveMode.Overwrite)
      .option("delimiter", "\t")
      .option("header", "true")
      .csv(outputDir)
  }

}
