import helpers.ReadTransformStoreHelper
import org.apache.spark.sql.SparkSession
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

class TopTenImdbMoviesTest extends FlatSpec with Matchers with BeforeAndAfter with ReadTransformStoreHelper {

  val spark = SparkSession.builder()
    .appName("TopTenImdbMoviesTest")
    .master("local")
    .getOrCreate()

  val inputDataDirectory = "src/test/resources/in/"
  val outputDataDirectory = "src/test/resources/out/"

  val allReadData = readData(spark, inputDataDirectory)

  val dfRatings = allReadData("ratingsDataFrame")
  dfRatings.cache()
  val dfBasicTitles = allReadData("titleBasicsDataFrame")

  val dfTopTenImdbMovies = getTopTenImdbMovies(dfRatings, dfBasicTitles)
  dfTopTenImdbMovies.cache()

  "After function getTopTenImdbMovies dataframe" should "have only ten rows" in {
    dfTopTenImdbMovies.count().shouldEqual(10)
  }

  "After function getTopTenImdbMovies dataframe" should "have only columns tconst, primaryTitle and ranking" in {
    dfTopTenImdbMovies.columns.shouldEqual(Array("tconst", "primaryTitle", "ranking"))
  }

  "After function getTopTenImdbMovies dataframe" should "have movie The Shawshank Redemption as the best rated Imdb Movie" in {
    dfTopTenImdbMovies.first().get(1).shouldEqual("The Shawshank Redemption")
    dfTopTenImdbMovies.first().get(2).shouldEqual(23663.55)
  }

}
