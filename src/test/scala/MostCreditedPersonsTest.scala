import helpers.ReadTransformStoreHelper
import org.apache.spark.sql.SparkSession
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import imdb.topTenMoviesSchema

class MostCreditedPersonsTest extends FlatSpec with Matchers with BeforeAndAfter with ReadTransformStoreHelper {

  val spark = SparkSession.builder()
    .appName("MostCreditedPersonsTest")
    .master("local")
    .getOrCreate()

  val inputDataDirectory = "src/test/resources/in/"
  val outputDataDirectory = "src/test/resources/out/"

  val allReadData = readData(spark, inputDataDirectory)

  val dfTopTenImdbMovies = createDF(spark, inputDataDirectory+"top_ten.csv", topTenMoviesSchema)
  dfTopTenImdbMovies.cache()

  val dfTopTenMoviesWithMostCreditedPersons = getTheMostOftenCreditedPeople(dfTopTenImdbMovies, allReadData)

  "After function getTheMostOftenCreditedPeople dataframe" should "have only columns nconst, primaryName and numberOfCredits" in {
    dfTopTenMoviesWithMostCreditedPersons.columns.shouldEqual(Array("nconst", "primaryName", "numberOfCredits"))
  }

  "After function getTheMostOftenCreditedPeople dataframe" should "have max number of credits 2" in {
    dfTopTenMoviesWithMostCreditedPersons.first().get(2).shouldEqual(2)
  }

}
