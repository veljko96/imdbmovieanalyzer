import helpers.ReadTransformStoreHelper
import org.apache.spark.sql.SparkSession
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}
import imdb.topTenMoviesSchema

class AltMoviesTitlesTest extends FlatSpec with Matchers with BeforeAndAfter with ReadTransformStoreHelper {

  val spark = SparkSession.builder()
    .appName("AltMoviesTitlesTest")
    .master("local")
    .getOrCreate()

  val inputDataDirectory = "src/test/resources/in/"
  val outputDataDirectory = "src/test/resources/out/"

  val allReadData = readData(spark, inputDataDirectory)

  val dfTopTenImdbMovies = createDF(spark, inputDataDirectory+"top_ten.csv", topTenMoviesSchema)
  dfTopTenImdbMovies.cache()

  val dfAllAltMovieTitles = getAllAltMovieTitles(dfTopTenImdbMovies, allReadData)

  "After function getAllAltMovieTitles dataframe" should "have only ten rows" in {
    dfAllAltMovieTitles.count().shouldEqual(10)
  }

  "After function getAllAltMovieTitles dataframe" should "have only columns tconst, primaryTitle, altNames" in {
    dfAllAltMovieTitles.columns.shouldEqual(Array("tconst", "primaryTitle", "altNames"))
  }

  "After function getAllAltMovieTitles dataframe" should "have expected altTitles for movie The Shawshank Redemption" in {
    dfAllAltMovieTitles.first().get(2).shouldEqual("Sueño de fuga,The Prisoner,A remény rabjai,Rita Hayworth - Avain pakoon,Втеча з Шоушенка,Vykúpenie z väznice Shawshank,Avain pakoon,ショーシャンクの空に,Rita Hayworth - nyckel till flykten,Teleftaia exodos: 'Rita Hayworth',À l'ombre de Shawshank,Cadena perpetua,En verden udenfor,Kaznilnica odrešitve,Выкупленьне з Шаўшэнку,Nyckeln till frihet,Os Condenados de Shawshank,Sueño de Libertad,月黑高飛,Shawshanki lunastus,Cadena perpètua,Le ali della libertà,Homot Shel Tikva,Închisoarea îngerilor,Sueños de Libertad,Pabėgimas iš Šoušenko,肖申克的救赎,Iskupljenje u Shawshanku,Shôshanku no sora ni,Shawshank Redemption - Avain pakoon,Shawshank Mittraphap Khwamwang Khwamrunraeng,Shoushenkden Qacish,De ontsnapping,Um Sonho de Liberdade,刺激1995,Vykoupení z věznice Shawshank,Rastegari dar Shawshank,Sueños de fuga,Die Verurteilten,The Shawshank Redemption,Les Évadés,Sueños de libertad,Изкуплението Шоушенк,Rita Hayworth and Shawshank Redemption,Skazani na Shawshank,Nhà tù Shawshank,Gaqceva shoushenkidan,Побег из Шоушенка,Shoushenkdan qochish,Frihetens regn,Sueño de libertad,Τελευταία έξοδος: Ρίτα Χέιγουορθ,Бекство из Шошенка,Esaretin Bedeli")
  }

}
