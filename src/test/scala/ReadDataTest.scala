import helpers.ReadTransformStoreHelper
import org.apache.spark.sql.SparkSession
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

class ReadDataTest extends FlatSpec with Matchers with BeforeAndAfter with ReadTransformStoreHelper {

  val spark = SparkSession.builder()
    .appName("ReadDataTest")
    .master("local")
    .getOrCreate()

  val inputDataDirectory = "src/test/resources/in/"

  val allReadData = readData(spark, inputDataDirectory)

  "After function allReadData Map" should "have 5 keys" in {
    allReadData.keys.size.shouldEqual(5)
  }

  "After function ratings dataframe" should "have 1212003 rows" in {
    val dfRatings = allReadData("ratingsDataFrame")
    dfRatings.count().shouldEqual(1212002)
  }

}
